import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qoranios/Activity/surelist_act.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
//import 'package:audioplayers/audioplayers.dart';
//import 'package:archive/archive.dart';
//import 'package:flutter_archive/flutter_archive.dart';
import 'dart:io';



class audiotest_act extends StatefulWidget
{
  DictionaryDataBaseHelper db;
  @override
  _audiotest_actState createState() => _audiotest_actState();
}

class _audiotest_actState extends State<audiotest_act>
{

  ///////////////////////////////////////AudioPlayer audioPlayer = AudioPlayer();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    widget.db=new DictionaryDataBaseHelper();
    widget.db.init();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Material(
      child: Stack(
        children: [
          Container(color: Colors.black,),
          Container(margin: EdgeInsets.fromLTRB(10, 50, 10, 0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(child: Icon(Icons.loop, color: Colors.white),onTap:()=>click(1) ,),
                InkWell(child: Icon(Icons.fast_forward, color: Colors.white),onTap:()=>click(2)),
                InkWell(child: Icon(Icons.play_arrow, color: Colors.white),onTap:()=>click(3)),
                InkWell(child: Icon(Icons.fast_rewind, color: Colors.white),onTap:()=>click(4)),
                InkWell(child: Icon(Icons.stop, color: Colors.white),onTap:()=>click(5)),
              ],
            ),
          )
        ],
      ),
    ),);
  }
  void click(int n) async//play=3    stop=5   loop=1
  {
    print(n);
    if(n==3)
      {
        List data=new List();
        data=await widget.db.getTestSure() as List ;
        print(data);
      }
    if(n==5)
      {
        //int res=await widget.db.updateDB();
        //print("res=$res");//1=succ
      }
    //playLocal(n);
  }
  playLocal(int n) async
  {
    //
    //var localPath="/data/data/com.hamrayan.qoranios/app_flutter/testaudio1.mp3";
    //int result = await audioPlayer.play(localPath, isLocal: true);

    /*if(n==1)
      {
        final zipFile = File("/data/data/com.hamrayan.qoranios/app_flutter/testsound.zip");
        final destinationDir = Directory("/data/data/com.hamrayan.qoranios/app_flutter/testsound");
        try {
          ZipFile.extractToDirectory(zipFile: zipFile, destinationDir: destinationDir);
        } catch (e) {
          print("eee=$e");
        }
      }
    if(n==3)
      {
        var localPath="/data/data/com.hamrayan.qoranios/app_flutter/testsound/1.mp3";
        int result = await audioPlayer.play(localPath, isLocal: true);
      }
    if(n==5)
      {
        int result = await audioPlayer.stop();
      }
    if(n==2)
      {
        _showDialog(context);
      }
    if(n==4)
    {
      showAlertDialog(context);
    }*/
  }
  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Alert!!"),
          content: new Text("You are awesome!"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
                print("ok");
              },
            ),
          ],
        );
      },
    );
  }
  showAlertDialog(BuildContext context)
  {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed:  () {Navigator.of(context).pop();},
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed:  () {Navigator.of(context).pop();},
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content: Text("Would you like to continue learning how to use Flutter alerts?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
