import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qoranios/Activity/surelist_act.dart';
import 'package:qoranios/Activity/video_act.dart';
import 'package:qoranios/Classes/database_helper%20.dart';

class list_act extends StatefulWidget
{
  DictionaryDataBaseHelper db;
  var item;
  int page;//0=[qoran,talavat,amuzesh]     1=[bayat,rast,nahavand,hejaz,segah,chahargah,saba]       2=[tajvid,tafkik,rukhani,ravankhani]    3=[namha,khatm,faliyat]
  list_act(var item, DictionaryDataBaseHelper db,int page)
  {
    this.item=item;
    this.db = db;
    this.page=page;
  }
  @override
  _list_actState createState() => _list_actState();
}

class _list_actState extends State<list_act> {
  //var item = ["قرآن کریم", "تلاوت مجلسی", "آموزش", "متفرقه", "ارتباط با ما"];
  //var item = ["قرآن کریم", "تلاوت مجلسی"];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  Widget build(BuildContext context)
  {

    return  Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/page2_bg.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 32.0,
            color: Color(0xBFfaf0d1),
            child: Align(
              child: Text(
                "قاری کبیر",
                style: TextStyle(fontSize: 10.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false),
              ),
              alignment: AlignmentDirectional.center,
            ),
          ),
          Align(
            alignment: AlignmentDirectional.center,
            child: Container(margin: EdgeInsets.all(60.0),
              decoration: new BoxDecoration(
                color: Color(0xFFfdfaf0),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: widget.item.length,
                itemBuilder: (context, position) {
                  return Material(borderRadius:BorderRadius.all(Radius.circular(10.0)) ,color:Color(0xFFfdfaf0) ,
                    child: InkWell(onTap:()=>list_click(position) ,
                      child: Column(children: [
                        Align(
                          alignment: AlignmentDirectional.center,
                          child: Container(
                            alignment: AlignmentDirectional.center,
                            height: 100.0,
                            child: Text(
                              widget.item[position],
                              style: TextStyle(
                                  fontSize: 20.0,
                                  color: Color(0xFF72716c),
                                  inherit: false,fontFamily: "grapic_ar"),
                            ),
                          ),
                        ),
                        Visibility(visible:(position!=widget.item.length-1) ,
                          child: Container(margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                            child: Divider(
                              height: 2.0,
                              color: Color(0xFF92582b),
                            ),
                          ),
                        ),
                      ]),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      );
  }
  void list_click(int posion)
  {
    print("posion=$posion");
    if(widget.page==0)
      {
        if(posion==0)
        {
          Navigator.push(
            context,
            //CupertinoPageRoute(builder: (context) => list_act()),
            MaterialPageRoute(builder: (context) => surelist_act(widget.db)),
          );
        }
        else if(posion==1)
        {
          var item = ["مقام بیات", "مقام رست", "مقام نهاوند", "مقام حجاز","مقام سه گاه","مقام چهارگاه","مقام صبا"];
          Navigator.push(
            context,
            //CupertinoPageRoute(builder: (context) => list_act()),
            MaterialPageRoute(builder: (context) => list_act(item,widget.db,1)),
          );
        }
        else if(posion==2)
        {
          var item = ["تجوید مقدماتی", "تفکیک روخوانی", "روخوانی","روانخوانی"];
          Navigator.push(
            context,
            //CupertinoPageRoute(builder: (context) => list_act()),
            MaterialPageRoute(builder: (context) => list_act(item,widget.db,2)),
          );
        }
        else if(posion==3)
        {
          var item = ["نام های خداوند","ختم نامه دعائیه","فعالیت های مدرسه","نهاد خیریه","دروس آنلاین"];
          Navigator.push(
            context,
            //CupertinoPageRoute(builder: (context) => list_act()),
            MaterialPageRoute(builder: (context) => list_act(item,widget.db,3)),
          );
        }
      }
    else if(widget.page==1)
      {
        String url="";
        if(posion==0)
          {
            url="https://s1.yousefi.ir/qarikabir/Moqama-Bayat.mp4";
          }
        else if(posion==1)
        {
          url="https://s1.yousefi.ir/qarikabir/Moqama-Rast.mp4";
        }
        else if(posion==2)
        {
          url="https://s1.yousefi.ir/qarikabir/Moqama-Nahawand.mp4";
        }
        else if(posion==3)
        {
          url="https://s1.yousefi.ir/qarikabir/Moqama-Hujaz.mp4";
        }
        else if(posion==4)
        {
          url="https://s1.yousefi.ir/qarikabir/Moqama-Sega.mp4";
        }
        else if(posion==5)
        {
          url="https://s1.yousefi.ir/qarikabir/Moqama-Charga.mp4";
        }
        else if(posion==6)
        {
          url="https://s1.yousefi.ir/qarikabir/Moqama-Saba.mp4";
        }
        Navigator.push(context,
          MaterialPageRoute(builder: (context) => video_act(url)),
        );
      }

  }
}



