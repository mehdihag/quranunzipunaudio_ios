import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:just_audio/just_audio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qoranios/Activity/surelist_act.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class matn2_act extends StatefulWidget {
  int type = 0; //1=sure   2=juz    3=manabe
  int index = 0;
  DictionaryDataBaseHelper db;
  List data = new List();

  matn2_act(int type, int index, DictionaryDataBaseHelper db) {
    this.type = type;
    this.index = index;
    this.db = db;
    print("type=$type    index=$index");

    //setData();
  }
  @override
  _matn2_actState createState() => _matn2_actState();
}

class _matn2_actState extends State<matn2_act> {
  bool visible_top = true;
  bool visible_bot = true;
  int _value = 1;
  String sure_name="";
  String sure_adr="";
  List sure_data = new List();
  //AudioPlayer player1;
  //AudioPlayer player2;
  bool isplaying=false;
  bool isfirstplay=true;
  var cur_icon=Icons.play_arrow;
  var icon_bookmark=Icons.bookmark_border;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    //player1 = AudioPlayer();
   // player1.setAsset('assets/audio/hamd.mp3');
    //player2 = AudioPlayer();
    setData();
  }

  setData() async {
    if (widget.type == 1)
    {
      sure_data = await widget.db.getCurSure(widget.index) as List;
      //print("sure_data=${sure_data[0]['arabic_name']}");
      sure_name="سوره ${sure_data[0]['arabic_name']}";
      sure_adr="صفحه ${sure_data[0]['start_page']}/جزء ${sure_data[0]['juz']}";
      widget.data = await widget.db.getSure(widget.index) as List;
      //print("bbbbb=$data");
      setState(() {});
      await Future.delayed(const Duration(seconds: 3), (){print("3333");
      visible_top=false;
      visible_bot=false;
      setState(() {

      });});
    }
    else if (widget.type == 2)
    {
      sure_data = await widget.db.getCurSure(widget.index) as List;
      //print("sure_data=${sure_data[0]['arabic_name']}");
      sure_name="جزء ${sure_data[0]['juz']}";

      widget.data = await widget.db.getJuz(widget.index) as List;
      int num=widget.data.length;
      sure_adr="تعداد آیات ${num}";
      //print("bbbbb=$data");
      setState(() {});
      await Future.delayed(const Duration(seconds: 3), (){print("3333");
      visible_top=false;
      visible_bot=false;
      setState(() {

      });});
    }
  }

  @override
  Widget build(BuildContext context) {
    print("data.length=${widget.data.length}");

    return MaterialApp(
      theme: new ThemeData(canvasColor: Colors.black),
      debugShowCheckedModeBanner: false,
      home: Directionality(
        textDirection: TextDirection.rtl,
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              color: Color(0xFFfef8ea),
            ),
            Container(
              width: double.infinity,
              height: 32.0,
              color: Color(0xFFfaf0d1),
              child: Align(
                child: Text(
                  "قاری کبیر",
                  style: TextStyle(fontSize: 10.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false),
                ),
                alignment: AlignmentDirectional.center,
              ),
            ),
            SingleChildScrollView(
              child: GestureDetector(
                onTap: (){showPanel();},
                child: Container(
                  margin: EdgeInsets.fromLTRB(5.0, 35.0, 5.0, 35.0),
                  child: RichText(
                    text: TextSpan(
                      style: TextStyle(fontFamily: "me_quran"),
                      children: getList(),
                    ),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: visible_top,
              child: Container(height: 54.0,width: double.infinity,
                child: Material(
                  child: Stack(
                    children: [Align(
                      alignment: Alignment.topCenter,
                      child: Material(
                        child: Container(
                          alignment: Alignment.centerRight,
                          width: double.infinity,
                          height: 54.0,
                          color: Color(0xFF645f5b),
                          child: Wrap(
                            spacing: 10.0,
                            children: [
                              PopupMenuButton(
                                child: Icon(Icons.more_vert, color: Colors.white),
                                itemBuilder: (context) => [
                                  PopupMenuItem(
                                    value: 1,
                                    child: Text("First"),
                                  ),
                                  PopupMenuItem(
                                    value: 2,
                                    child: Text("Second"),
                                  ),
                                ],
                              ),
                              InkWell (
                                child: Icon(icon_bookmark, color: Colors.white),
                                onTap: (){setBookmark();setState(() {

                                });}

                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    //Container(height: 54.0,child: Align(alignment: Alignment.topCenter,child: Wrap(direction: Axis.vertical,spacing: 5.0,children: [Text("سوره الروم",style: TextStyle(fontSize: 14.0, color: Colors.white,fontFamily: "grapic_ar",inherit: false)),Text("صفحه404/جزء 21",style: TextStyle(fontSize: 12.0, color: Colors.white,fontFamily: "grapic_ar",inherit: false))],))),
                      Container(padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),height: 54.0,child:Align(alignment: Alignment.topCenter,child: Text(sure_name,style: TextStyle(fontSize: 14.0, color: Colors.white,fontFamily: "grapic_ar",inherit: false))) ,),
                      Container(padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),height: 54.0,child:Align(alignment: Alignment.bottomCenter,child: Text(sure_adr,style: TextStyle(fontSize: 12.0, color: Colors.white,fontFamily: "grapic_ar",inherit: false))) ,),
                      //Material(child: Container(height: 54.0,child: Align(alignment: Alignment.center,child: InkWell  ( child: Icon(Icons.keyboard_arrow_right, color: Colors.white),onTap: (){print("tttt");},)),)),
                      Align(alignment: Alignment.center,child: InkWell(child: Container(margin: EdgeInsets.fromLTRB(100.0, 0.0, 0.0, 0.0),height: 54.0,child: Icon(Icons.keyboard_arrow_right, color: Colors.white)),onTap: (){print("vvvvv");nextSure();},))
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: visible_bot,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: double.infinity,
                  height: 54.0,
                  //color: Color(0xFF645f5b),
                  color: Colors.white,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: FlatButton(
                          height: double.infinity,
                          onPressed: () {
                            play_ayat();
                            //playAll();
                          },
                          child: Icon(cur_icon, color: Colors.white),
                          color: Color(0xFF645f5b),
                        ),
                        flex: 1,
                      ),
                      Expanded(
                        child: Container(
                          height: double.infinity,
                          margin: EdgeInsets.fromLTRB(2.0, 0, 2.0, 0.0),
                          padding: EdgeInsets.fromLTRB(0.0, 0, 7.0, 0.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(0.0),
                              color: Color(0xFF645f5b),
                              border: null), //BoxDecoration
                          child: DropdownButtonHideUnderline(
                            child: Material(
                              color: Color(0xFF645f5b),
                              child: DropdownButton(
                                  value: _value,
                                  items: [
                                    DropdownMenuItem(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                "قرائت ترتیل(استاد عبدالکبیر حیدری الافغانی)",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 11.0,
                                                    fontFamily: "grapic_ar"),
                                                textAlign: TextAlign.right)),
                                        value: 1),
                                    DropdownMenuItem(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                "قرائت تحدیر(استاد عبدالکبیر حیدری الافغانی)",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 11.0,
                                                    fontFamily: "grapic_ar"),
                                                textAlign: TextAlign.right)),
                                        value: 2),
                                    DropdownMenuItem(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                "قرائت تدویر(استاد عبدالکبیر حیدری الافغانی)",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 11.0,
                                                    fontFamily: "grapic_ar"),
                                                textAlign: TextAlign.right)),
                                        value: 3),
                                    DropdownMenuItem(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                "قرائت مجلسی(استاد عبدالکبیر حیدری الافغانی)",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 11.0,
                                                    fontFamily: "grapic_ar"),
                                                textAlign: TextAlign.right)),
                                        value: 4),
                                    DropdownMenuItem(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                              "ترجمه فارسی دری(استاد عبدالکبیر حیدری الافغانی)",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10.0,
                                                  fontFamily: "grapic_ar"),
                                              textAlign: TextAlign.right,
                                            )),
                                        value: 5),
                                    DropdownMenuItem(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                              "ترجمه پشتو(استاد عبدالکبیر حیدری الافغانی)",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 11.0,
                                                  fontFamily: "grapic_ar"),
                                              textAlign: TextAlign.right,
                                            )),
                                        value: 6),
                                    DropdownMenuItem(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                "ترجمه انگلیسی(استاد عبدالکبیر حیدری الافغانی)",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 11.0,
                                                    fontFamily: "grapic_ar"),
                                                textAlign: TextAlign.right)),
                                        value: 7),
                                  ],
                                  onChanged: (value) {
                                    setState(() {
                                      _value = value;
                                    });
                                  }),
                            ), //DropdownButton
                          ), //DropdownButtonHideUnderline
                        ),
                        flex: 4,
                      ),
                      Expanded(
                        child: FlatButton(
                          height: double.infinity,
                          onPressed: () {
                            print("down");
                          },
                          child: Icon(Icons.keyboard_arrow_down,
                              color: Colors.white),
                          color: Color(0xFF645f5b),
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<TextSpan> getList()
  {

    List<TextSpan> spans = [];
    //spans.add(TextSpan(text: "aaa",style: TextStyle(color: Colors.red, fontSize: 18)));
    //spans.add(TextSpan(text: " bbb",style: TextStyle(color: Colors.blue, fontSize: 18)));
    if (widget.index == 1) //hamd
    {
      for (int i = 0; i < widget.data.length; i++) {
        //print(widget.data[i]['text']);
        spans.add(TextSpan(
            text: widget.data[i]['text'] +
                "﴿" +
                widget.data[i]['aya'].toString() +
                "﴾",
            style: TextStyle(color: Colors.black, fontSize: 18)));
      }
    } else {
      for (int i = 0; i < widget.data.length; i++) {
        if (i == 0)
          spans.add(TextSpan(
              text: widget.data[0]['text'] + "\n ",
              style: TextStyle(color: Colors.black, fontSize: 18)));
        else {
          spans.add(TextSpan(
              text: widget.data[i]['text'] +
                  "﴿" +
                  widget.data[i]['aya'].toString() +
                  "﴾",
              style: TextStyle(color: Colors.black, fontSize: 18)));
        }
      }
    }

    return spans;
  }
  void play_ayat() async
  {

    /*if(widget.type==1 && widget.index==1)
      {
        if(isplaying==false )
          {
            if(isfirstplay==true)
              {
                isplaying=true;
                cur_icon=Icons.stop;
                isfirstplay=false;
                player1.play();

              }
            else
              {
                isplaying=true;
                cur_icon=Icons.stop;
                isfirstplay=false;
                player1.play();
              }
          }
        else
          {
            isplaying=false;
            cur_icon=Icons.play_arrow;
            player1.pause();
          }
        setState(() {

        });
      }
*/



  }
  void playAll() async
  {
    /*await player1.setAsset('assets/audio/001001.mp3');
    await player2.setAsset('assets/audio/001002.mp3');

    player1.play();
    await Future.delayed(const Duration(seconds: 6), ()
    {
      print("3333");
      player2.play();
    });
*/
  }
  void nextSure()
  {
    if(widget.type==1)
      {
        cur_icon=Icons.play_arrow;
        widget.index++;
        if(widget.index>114)
          {
            widget.index=1;
          }
        widget.data = new List();
        setData();
      }
    else if(widget.type==2)
    {
      cur_icon=Icons.play_arrow;
      widget.index++;
      if(widget.index>30)
      {
        widget.index=1;
      }
      widget.data = new List();
      setData();
    }
  }
  void showPanel() async
  {
    visible_top=true;
    visible_bot=true;
    setState(() {

    });
    await Future.delayed(const Duration(seconds: 3), (){print("3333");
    visible_top=false;
    visible_bot=false;
    setState(() {

    });});
  }
  void setBookmark()
  {
    if(icon_bookmark==Icons.bookmark_border)
      {
        icon_bookmark=Icons.bookmark;
      }
    else
      {
        icon_bookmark=Icons.bookmark_border;
      }
  }

}
