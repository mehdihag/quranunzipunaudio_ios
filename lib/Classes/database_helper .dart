import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'dart:io' as io;


class DictionaryDataBaseHelper {
  Database _db;

  Future<void> init() async {
    io.Directory applicationDirectory =
    await getApplicationDocumentsDirectory();

    String dbPathEnglish =
    path.join(applicationDirectory.path, "quran.db");

    bool dbExistsEnglish = await io.File(dbPathEnglish).exists();
    print("dbExistsEnglish=$dbExistsEnglish");
    if (!dbExistsEnglish) {
      // Copy from asset
      ByteData data = await rootBundle.load(
          path.join("assets/db", "quran.db"));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await io.File(dbPathEnglish).writeAsBytes(bytes, flush: true);

      print("copy copy copy copy copy ");
    }
    print("1111111111111111111111");
    this._db = await openDatabase(dbPathEnglish);
    print("2222222222222222222");

  }
   getAll(String table_name) async
  {
    print("3333333333333333333333333");
    var res = await _db.rawQuery("SELECT * FROM "+table_name);
    print("aaaa");
    //print("res=$res");
    print("*************************");
    //print(res[0]["name"]);//فاتحه
    //print(res[1]["name"]);//بقره

    /*List ls=new List();
    ls=res;
    return ls;*/
    return res.toList();
  }
  Future<List> getAll2(String table_name,String str,int type) async   //type=1==>sure    type=2==>juz
  {
    Future<List> res;
    if(type==1)
      {
        res =  _db.rawQuery("SELECT * FROM "+table_name+" where name LIKE '%"+str+"%' OR arabic_name LIKE '%"+str+"%' ");
      }
    else if(type==2)
      {
        res =  _db.rawQuery("SELECT * FROM "+table_name+" where id LIKE '%"+str+"%'  ");
      }
    List myrow=await res;
    return myrow;
  }
  Future<List> getSure(int sure_index) async
  {
    Future<List> res;
    if(sure_index==1)//hamd
      {
        res =  _db.rawQuery("SELECT * FROM quran_text where sura="+sure_index.toString());
      }
    else if(sure_index==9)//tobe
      {
        res =  _db.rawQuery("SELECT * FROM quran_text where sura="+sure_index.toString());
      }
    else
      {
        //res =  _db.rawQuery("SELECT * FROM quran_text where id=1 or sura="+sure_index.toString()+" order by id");
        res =  _db.rawQuery("SELECT * FROM quran_text where  sura="+sure_index.toString()+" order by id");
      }

    List myrow=await res;
    return myrow;
  }
  Future<List> getCurSure(int sure_index) async
  {
    Future<List> res;
    res =  _db.rawQuery("SELECT * FROM sura where id="+sure_index.toString());

    List myrow=await res;
    return myrow;
  }
  Future<List> getJuz(int juz_index) async
  {
    Future<List> res;
    res =  _db.rawQuery("SELECT * FROM arabic_text where juz="+juz_index.toString());

    List myrow=await res;
    return myrow;
  }
  Future<List> getTestSure() async
  {
    Future<List> res;
    res =  _db.rawQuery("SELECT * FROM arabic_text where _id='1'");

    List myrow=await res;
    return myrow;
  }
  Future<int> updateDB(int index,int type,int n) async
  {
    int count=0;
    //n=0==>not fav   n=1==>fav
    if(type==1)//sure
      {
         count = await _db.rawUpdate('UPDATE sura SET fav = ? WHERE id = ?', [n, index]);
      }
    else if(type==2)//juz
      {
         count = await _db.rawUpdate('UPDATE juz_page SET fav = ? WHERE id = ?', [n, index]);
      }
    //int count = await _db.rawUpdate('UPDATE arabic_text SET name = ?, value = ? WHERE name = ?', ['updated name', '9876', 'some name']);
    //int count = await _db.rawUpdate('UPDATE arabic_text SET audio = ? WHERE _id = ?', ['updated name', 1]);

    return count;
  }
  Future<List> getFav(String table) async
  {
    Future<List> res;
    res =  _db.rawQuery("SELECT * FROM $table where fav='1'");

    List myrow=await res;
    return myrow;
  }
  Future<List> getSearch(String str) async
      {
    Future<List> res;
    //res =  _db.rawQuery("SELECT * FROM arabic_text where text LIKE '%"+str+"%' OR search_text LIKE '%"+str+"%' ");
    res =  _db.rawQuery("SELECT * FROM arabic_text INNER JOIN sura on arabic_text.sura=sura.id where text LIKE '%"+str+"%' OR search_text LIKE '%"+str+"%' ");

    List myrow=await res;
    return myrow;
  }
}